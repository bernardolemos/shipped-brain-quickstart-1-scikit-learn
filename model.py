import joblib
import numpy as np
import pandas as pd
from typing import Union

class UserModel:

    def __init__(self, model_path):
        self.model = joblib.load(model_path)

    def predict(self, 
                input_features: pd.DataFrame) -> Union[np.ndarray, pd.Series, pd.DataFrame]:
        return self.model.predict(input_features)